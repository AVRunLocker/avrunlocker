# What is this?
Simply it is device, that can "unlock" blocked AVR. Sometimes every user set badly fuses. As result this settings can be set wrong oscillator, disabled RESET, disabled SPI programming and so on and re-programming is not longer possible. And for these cases there is AVR unLocker! This device can reset target AVR to factory default, so you can re-program it again. Note that device can erase ALL MEMORIES, so be sure you have proper backup! Here is photo.

 ![Logo](https://gitlab.com/AVRunLocker/doc/raw/master/logo.jpg)

# Download
 * To download HW design, SW and documentation, just use good old GIT clone

```bash
$ git clone --recursive git@gitlab.com:AVRunLocker/avrunlocker.git
```

# How it works?
Simply :) Heavy simplified flowchart is on following image.

 ![Block scheme](https://gitlab.com/AVRunLocker/doc/raw/master/block_scheme_en.png)

 After initialization device try communicate over SPI. If communication with target chip will be successful, then it is high chance to set fuses back to factory settings without erasing flash or EEPROM memory -> code stay.
   When unlock over SPI fails, then is need to erase lock bits and fuses (factory settings). However, this means, that flash and EEPROM must be erased too. For this is suitable high voltage programming.
    If even high voltage programming fails, then whole process is done again. This is for case, that user did not put target to IC socket, so no restart is needed. When device try communicate with target, then trying all oscillator types. This is for case, that user just set wrong oscillator. For example: if in target IC is set RC oscillator, then target IC will not work (no clock source) until we connect right RC oscillator to pins.
   In following table is overview HW and SW versions. Hardware is backwards compatible. Actual firmware is written in C language, because in this application MCU most of time just waiting for target, so there are not time critical routines -> assembler is not much suitable for this application. Database is not complete, yet. At this moment most of ATtiny and Xmega are not in database. Also missing serial high voltage programming. However, sometimes I work on database, but expand database is very easy.Just edit file "chip_database.c" and recompile whole project.

# Demo videos
## Unlocking ATtiny13 - RC oscillator set
 [Unlocking ATtiny13 - RC oscillator set](https://gitlab.com/AVRunLocker/doc/raw/master/Video/unLocker_ATtiny13.ogg)

## Unlocking ATmega32 - external clock set
 [Unlocking ATmega32 - external clock set](https://gitlab.com/AVRunLocker/doc/raw/master/Video/unLocker_ATmega32.ogg)

## Unlocking ATmega8 - reset: disabled , SPI: disabled
 [Unlocking ATmega8 - reset: disabled , SPI: disabled](https://gitlab.com/AVRunLocker/doc/raw/master/Video/unLocker_ATmega8.ogg)

Because RESET and SPI were disabled, high voltage programming mode were used to communicate with target (ATmega8).

# Conclusion
If You use correct components and whole HW will be correct, device should work. Only one thing should (do not have to) be set: trimmer for setting contrast on LCD, which do not have to be even connected.
  As You can notice, display is up side down. That is because in first design I do not know that are two types of this display. So, if somebody create own version, just share it.
  If You have any questions, use email ;-)

* Project was originally "AVR unBlocker", however I renamed it to
  "AVR unLocker". That is why in old versions is different name for this
  project.
